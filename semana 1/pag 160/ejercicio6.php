<?php
$kilos = $_POST['kilos'];

if ($kilos >= 0 && $kilos <= 2) {
    $descuento = 0;
} elseif ($kilos > 2 && $kilos <= 5) {
    $descuento = 0.1;
} elseif ($kilos > 5 && $kilos <= 10) {
    $descuento = 0.2;
} else {
    $descuento = 0.3;
}

$precio_kilo = 2;  
$precio_total = $kilos * $precio_kilo;
$descuento_total = $precio_total * $descuento;
$precio_final = $precio_total - $descuento_total;
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Frutería</title>
</head>
<body>
    <h1>Frutería</h1>
    <form method="post">
        <label for="kilos">Cantidad de kilos comprados:</label>
        <input type="number" id="kilos" name="kilos" min="0" step="0.01" required>
        <button type="submit">Calcular precio</button>
    </form>
    <?php if (isset($precio_final)): ?>
        <p>El precio a pagar es: $<?php echo number_format($precio_final, 2); ?></p>
    <?php endif; ?>
</body>
</html>
