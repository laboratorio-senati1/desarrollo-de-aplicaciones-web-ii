<!DOCTYPE html>
<html>
<head>
	<title>Obtener estado civil</title>
</head>
<body>
	<h1>Obtener estado civil</h1>
	<form action="" method="post">
		<label for="codigo">Ingrese el código del estado civil (0-3):</label>
		<input type="number" id="codigo" name="codigo" min="0" max="3">
		<input type="submit" value="Obtener">
	</form>
	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$codigo = $_POST["codigo"];
			switch ($codigo) {
				case 0:
					echo "Estado civil: Soltero";
					break;
				case 1:
					echo "Estado civil: Casado";
					break;
				case 2:
					echo "Estado civil: Divorciado";
					break;
				case 3:
					echo "Estado civil: Viudo";
					break;
				default:
					echo "Código inválido";
			}
		}
	?>
</body>
</html>
