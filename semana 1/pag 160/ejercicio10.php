<!DOCTYPE html>
<html>
<head>
	<title>Calcular días hasta fin de año</title>
</head>
<body>
	<h1>Calcular días hasta fin de año</h1>
	<form method="post" action="">
		<label>Ingrese una fecha (dd/mm/aaaa):</label>
		<input type="text" name="fecha" required>
		<button type="submit">Calcular</button>
	</form>
	<?php
		if (isset($_POST['fecha'])) {
			$fecha = $_POST['fecha'];
			$fecha_actual = strtotime(date('d-m-Y'));
			$fecha_ingresada = strtotime($fecha);
			$diferencia = $fecha_ingresada - $fecha_actual;
			$dias_faltantes = 365 - (int)date('z', $fecha_ingresada);
			if ($diferencia < 0) {
				echo "<p>La fecha ingresada es anterior a la fecha actual</p>";
			} else {
				echo "<p>Faltan $dias_faltantes días para que acabe el año.</p>";
			}
		}
	?>
</body>
</html>
