<!DOCTYPE html>
<html>
<head>
	<title>Descuento de sueldo</title>
	<style>
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
			padding: 5px;
		}
	</style>
</head>
<body>
	<h1>Descuento de sueldo según género y categoría</h1>
	<form method="post">
		<label for="sueldo">Sueldo: </label>
		<input type="number" name="sueldo" id="sueldo"><br><br>
		<label for="genero">Género: </label>
		<select name="genero" id="genero">
			<option value="hombre">Hombre</option>
			<option value="mujer">Mujer</option>
		</select><br><br>
		<label for="categoria">Categoría: </label>
		<select name="categoria" id="categoria">
			<option value="obrero">Obrero</option>
			<option value="empleado">Empleado</option>
		</select><br><br>
		<input type="submit" value="Calcular descuento">
	</form><br>

	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$sueldo = $_POST["sueldo"];
			$genero = $_POST["genero"];
			$categoria = $_POST["categoria"];
			$descuento = 0;

			if ($genero == "hombre" && $categoria == "obrero") {
				$descuento = $sueldo * 0.15;
			} elseif ($genero == "mujer" && $categoria == "obrero") {
				$descuento = $sueldo * 0.10;
			} elseif ($genero == "hombre" && $categoria == "empleado") {
				$descuento = $sueldo * 0.20;
			} elseif ($genero == "mujer" && $categoria == "empleado") {
				$descuento = $sueldo * 0.15;
			}

			echo "<h2>El descuento de sueldo es: $" . number_format($descuento, 2) . "</h2>";
		}
	?>

</body>
</html>
