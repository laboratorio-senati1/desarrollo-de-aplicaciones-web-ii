<?php
if(isset($_POST['canal'])) {
  $canal = $_POST['canal'];
  switch($canal) {
    case 1:
      $nombre_canal = "televisa";
      break;
    case 2:
      $nombre_canal = "cartoonNetwork";
      break;
    case 3:
      $nombre_canal = "Fox";
      break;
    case 4:
      $nombre_canal = "Fox Sport";
      break;
    default:
      $nombre_canal = "Canal no válido";
  }
}
?>

<html>
  <head>
    <title>Nombre del Canal</title>
  </head>
  <body>
    <form method="post" action="">
      <label for="canal">Ingrese número de canal:</label>
      <input type="number" name="canal" id="canal" min="1" max="4">
      <button type="submit">Enviar</button>
    </form>
    <?php
    if(isset($nombre_canal)) {
      echo "El canal es: " . $nombre_canal;
    }
    ?>
  </body>
</html>
