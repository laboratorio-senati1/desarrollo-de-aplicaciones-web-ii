<!DOCTYPE html>
<html>
<head>
	<title>Visita según puntaje de examen</title>
</head>
<body>
	<h1>Visita según puntaje de examen</h1>
	<form method="POST">
		<label for="sexo">Sexo:</label>
		<select id="sexo" name="sexo">
			<option value="M">Masculino</option>
			<option value="F">Femenino</option>
		</select>
		<br>
		<label for="puntaje">Puntaje obtenido:</label>
		<input type="number" id="puntaje" name="puntaje" required>
		<br>
		<input type="submit" value="Calcular">
	</form>
	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$sexo = $_POST["sexo"];
			$puntaje = $_POST["puntaje"];
			$ciudad = "";
			if ($sexo == "M") {
				if ($puntaje >= 18 && $puntaje <= 35) {
					$ciudad = "Arequipa";
				} elseif ($puntaje >= 36 && $puntaje <= 75) {
					$ciudad = "Cuzco";
				} elseif ($puntaje > 75) {
					$ciudad = "Iquitos";
				}
			} elseif ($sexo == "F") {
				if ($puntaje >= 18 && $puntaje <= 35) {
					$ciudad = "Cuzco";
				} elseif ($puntaje >= 36 && $puntaje <= 75) {
					$ciudad = "Iquitos";
				} elseif ($puntaje > 75) {
					$ciudad = "Arequipa";
				}
			}
			if ($ciudad != "") {
				echo "<p>La ciudad que visitará es: $ciudad</p>";
			} else {
				echo "<p>No se pudo determinar la ciudad que visitará.</p>";
			}
		}
	?>
</body>
</html>
