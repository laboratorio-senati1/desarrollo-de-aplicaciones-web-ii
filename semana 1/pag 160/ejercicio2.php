<!DOCTYPE html>
<html>
<head>
	<title>Día de la semana</title>
</head>
<body>
	<form method="post">
		<label for="numero">Ingrese un número del 1 al 7:</label>
		<input type="number" name="numero" id="numero">
		<button type="submit">Enviar</button>
	</form>
	<?php
	if(isset($_POST['numero'])){
		$numero = $_POST['numero'];
		switch($numero){
			case 1:
				echo "<p>El número $numero corresponde al día domingo.</p>";
				break;
			case 2:
				echo "<p>El número $numero corresponde al día lunes.</p>";
				break;
			case 3:
				echo "<p>El número $numero corresponde al día martes.</p>";
				break;
			case 4:
				echo "<p>El número $numero corresponde al día miércoles.</p>";
				break;
			case 5:
				echo "<p>El número $numero corresponde al día jueves.</p>";
				break;
			case 6:
				echo "<p>El número $numero corresponde al día viernes.</p>";
				break;
			case 7:
				echo "<p>El número $numero corresponde al día sábado.</p>";
				break;
			default:
				echo "<p>Ingrese un número válido del 1 al 7.</p>";
		}
	}
	?>
</body>
</html>
