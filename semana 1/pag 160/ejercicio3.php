<!DOCTYPE html>
<html>
<head>
	<title>Operadores aritméticos</title>
</head>
<body>
	<form method="post">
		<label for="operador">Ingrese un operador aritmético (+, -, *, /):</label>
		<input type="text" name="operador">
		<input type="submit" value="Enviar">
	</form>

	<?php
		if (isset($_POST['operador'])) {
			$operador = $_POST['operador'];

			switch ($operador) {
				case '+':
					echo "El operador ingresado es Suma (+)";
					break;
				case '-':
					echo "El operador ingresado es Resta (-)";
					break;
				case '*':
					echo "El operador ingresado es Multiplicación (*)";
					break;
				case '/':
					echo "El operador ingresado es División (/)";
					break;
				default:
					echo "El operador ingresado no es válido";
					break;
			}
		}
	?>
</body>
</html>
