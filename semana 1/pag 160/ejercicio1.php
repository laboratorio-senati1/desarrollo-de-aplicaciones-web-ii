<!DOCTYPE html>
<html>
<head>
	<title>Mes en letras</title>
</head>
<body>
	<form method="post">
		<label for="mes">Ingresa el número del mes:</label>
		<input type="number" id="mes" name="mes">
		<button type="submit">Enviar</button>
	</form>
	<?php
		if(isset($_POST['mes'])){
			$mes = $_POST['mes'];
			switch($mes){
				case 1:
					echo "<p>Enero</p>";
					break;
				case 2:
					echo "<p>Febrero</p>";
					break;
				case 3:
					echo "<p>Marzo</p>";
					break;
				case 4:
					echo "<p>Abril</p>";
					break;
				case 5:
					echo "<p>Mayo</p>";
					break;
				case 6:
					echo "<p>Junio</p>";
					break;
				case 7:
					echo "<p>Julio</p>";
					break;
				case 8:
					echo "<p>Agosto</p>";
					break;
				case 9:
					echo "<p>Septiembre</p>";
					break;
				case 10:
					echo "<p>Octubre</p>";
					break;
				case 11:
					echo "<p>Noviembre</p>";
					break;
				case 12:
					echo "<p>Diciembre</p>";
					break;
				default:
					echo "<p>Ingrese un número de mes válido</p>";
					break;
			}
		}
	?>
</body>
</html>
