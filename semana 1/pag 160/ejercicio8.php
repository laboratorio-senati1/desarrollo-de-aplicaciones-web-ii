<!DOCTYPE html>
<html>
<head>
	<title>Calcular utilidades</title>
	<meta charset="utf-8">
</head>
<body>
	<h1>Calcular utilidades</h1>
	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<label for="tiempo_servicio">Tiempo de servicio:</label>
		<input type="number" name="tiempo_servicio" required>
		<br><br>
		<label for="cargo">Cargo:</label>
		<select name="cargo" required>
			<option value="">Seleccionar</option>
			<option value="Administrador">Administrador</option>
			<option value="Contador">Contador</option>
			<option value="Empleado">Empleado</option>
		</select>
		<br><br>
		<input type="submit" name="calcular" value="Calcular">
	</form>

	<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$tiempo_servicio = $_POST["tiempo_servicio"];
		$cargo = $_POST["cargo"];

		if ($tiempo_servicio >= 0 && $tiempo_servicio <= 2) {
			switch ($cargo) {
				case 'Administrador':
					$utilidades = 2000;
					break;
				case 'Contador':
					$utilidades = 1500;
					break;
				case 'Empleado':
					$utilidades = 1000;
					break;
				default:
					$utilidades = 0;
					break;
			}
		} elseif ($tiempo_servicio >= 3 && $tiempo_servicio <= 5) {
			switch ($cargo) {
				case 'Administrador':
					$utilidades = 2500;
					break;
				case 'Contador':
					$utilidades = 2000;
					break;
				case 'Empleado':
					$utilidades = 1500;
					break;
				default:
					$utilidades = 0;
					break;
			}
		} elseif ($tiempo_servicio >= 6 && $tiempo_servicio <= 8) {
			switch ($cargo) {
				case 'Administrador':
					$utilidades = 3000;
					break;
				case 'Contador':
					$utilidades = 2500;
					break;
				case 'Empleado':
					$utilidades = 2000;
					break;
				default:
					$utilidades = 0;
					break;
			}
		} elseif ($tiempo_servicio > 8) {
			switch ($cargo) {
				case 'Administrador':
					$utilidades = 4000;
					break;
				case 'Contador':
					$utilidades = 3500;
					break;
				case 'Empleado':
					$utilidades = 1500;
					break;
				default:
					$utilidades = 0;
					break;
			}
		} else {
			$utilidades = 0;
		}

		echo "<p>El trabajador recibirá un monto de $utilidades BsS por concepto de utilidades.</p>";
	}
	?>

</body>
</html
