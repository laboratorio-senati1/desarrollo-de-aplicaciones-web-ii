<!DOCTYPE html>
<html>
<head>
	<title>Promedio de notas</title>
</head>
<body>
	<h1>Promedio de notas</h1>
	<form method="post">
		<label for="nota1">Nota 1:</label>
		<input type="number" name="nota1" id="nota1" required>
		<br>
		<label for="nota2">Nota 2:</label>
		<input type="number" name="nota2" id="nota2" required>
		<br>
		<label for="nota3">Nota 3:</label>
		<input type="number" name="nota3" id="nota3" required>
		<br>
		<label for="nota4">Nota 4:</label>
		<input type="number" name="nota4" id="nota4" required>
		<br><br>
		<input type="submit" value="Calcular">
	</form>

	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$notas = array($_POST["nota1"], $_POST["nota2"], $_POST["nota3"], $_POST["nota4"]);
			rsort($notas); 
			$promedio = ($notas[0] + $notas[1] + $notas[2]) / 3; 
			if ($promedio >= 11) {
				echo "<p>El alumno aprobó con un promedio de " . round($promedio, 2) . "</p>";
			} else {
				echo "<p>El alumno desaprobó con un promedio de " . round($promedio, 2) . "</p>";
			}
		}
	?>
</body>
</html>
