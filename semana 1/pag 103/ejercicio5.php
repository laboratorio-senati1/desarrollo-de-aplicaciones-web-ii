<?php
if(isset($_POST['submit'])) {
    $num1 = $_POST['num1'];
    $num2 = $_POST['num2'];
    $num3 = $_POST['num3'];
        $ascendente = [$num1, $num2, $num3];
    sort($ascendente);
    $descendente = [$num1, $num2, $num3];
    rsort($descendente);
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Ordenar números enteros</title>
</head>
<body>
	<form method="post">
		<label for="num1">Primer número:</label>
		<input type="number" id="num1" name="num1"><br><br>

		<label for="num2">Segundo número:</label>
		<input type="number" id="num2" name="num2"><br><br>

		<label for="num3">Tercer número:</label>
		<input type="number" id="num3" name="num3"><br><br>

		<input type="submit" name="submit" value="Ordenar"><br><br>

		<?php if(isset($_POST['submit'])): ?>
			<p>Números ordenados en forma ascendente: <?php echo implode(', ', $ascendente); ?></p>
			<p>Números ordenados en forma descendente: <?php echo implode(', ', $descendente); ?></p>
		<?php endif; ?>
	</form>
</body>
</html>
