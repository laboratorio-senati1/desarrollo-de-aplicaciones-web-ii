<!DOCTYPE html>
<html>
<head>
	<title>Determinar el número mayor</title>
</head>
<body>
	<h1>Determinar el número mayor</h1>
	<form method="post">
		<label for="a">Ingrese el primer número:</label>
		<input type="number" id="a" name="a" required>
		<br><br>
		<label for="b">Ingrese el segundo número:</label>
		<input type="number" id="b" name="b" required>
		<br><br>
		<input type="submit" value="Calcular">
	</form>
	<?php
	if(isset($_POST['a']) && isset($_POST['b'])){
		$a = $_POST['a'];
		$b = $_POST['b'];
		if($a > $b){
			echo "<p>$a es mayor que $b</p>";
		}elseif($b > $a){
			echo "<p>$b es mayor que $a</p>";
		}else{
			echo "<p>$a y $b son iguales</p>";
		}
	}
	?>
</body>
</html>
