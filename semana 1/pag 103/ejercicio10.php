<!DOCTYPE html>
<html>
<head>
	<title>Tipo de Triángulo</title>
</head>
<body>
	<form method="post">
		<label>Ingrese la longitud del primer lado:</label>
		<input type="number" name="lado1"><br><br>
		<label>Ingrese la longitud del segundo lado:</label>
		<input type="number" name="lado2"><br><br>
		<label>Ingrese la longitud del tercer lado:</label>
		<input type="number" name="lado3"><br><br>
		<input type="submit" name="submit" value="Determinar tipo de triángulo">
	</form>

	<?php
	if(isset($_POST['submit'])){
		$lado1 = $_POST['lado1'];
		$lado2 = $_POST['lado2'];
		$lado3 = $_POST['lado3'];

		if($lado1 < ($lado2 + $lado3) && $lado2 < ($lado1 + $lado3) && $lado3 < ($lado1 + $lado2)){
			if($lado1 == $lado2 && $lado2 == $lado3){
				echo "Es un triángulo equilátero.";
			}elseif($lado1 == $lado2 || $lado1 == $lado3 || $lado2 == $lado3){
				echo "Es un triángulo isósceles.";
			}else{
				echo "Es un triángulo escaleno.";
			}
		}else{
			echo "Las longitudes ingresadas no forman un triángulo.";
		}
	}
	?>
</body>
</html>
