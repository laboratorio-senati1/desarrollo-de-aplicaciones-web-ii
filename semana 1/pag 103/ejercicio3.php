<!DOCTYPE html>
<html>
<head>
	<title>Verificar si dos números son iguales o diferentes</title>
</head>
<body>
	<h2>Verificar si dos números son iguales o diferentes</h2>
	<form method="post">
		<label>Ingrese el primer número:</label>
		<input type="number" name="num1"><br><br>
		<label>Ingrese el segundo número:</label>
		<input type="number" name="num2"><br><br>
		<input type="submit" name="submit" value="Verificar">
	</form>
	<?php
		if(isset($_POST['submit'])){
			$num1 = $_POST['num1'];
			$num2 = $_POST['num2'];
			if($num1 == $num2){
				echo "<p>Los números ingresados son iguales.</p>";
			}else{
				echo "<p>Los números ingresados son diferentes.</p>";
			}
		}
	?>
</body>
</html>
