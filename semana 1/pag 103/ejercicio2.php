<!DOCTYPE html>
<html>
<head>
	<title>Encontrar el número menor</title>
</head>
<body>
	<h1>Encontrar el número menor</h1>

	<form method="post" action="">
		<label for="num1">Ingresa el primer número entero:</label>
		<input type="number" name="num1" id="num1" required>
		<br><br>
		<label for="num2">Ingresa el segundo número entero:</label>
		<input type="number" name="num2" id="num2" required>
		<br><br>
		<input type="submit" value="Encontrar el número menor">
	</form>

	<?php
	if (isset($_POST["num1"]) && isset($_POST["num2"])) {
		$num1 = $_POST["num1"];
		$num2 = $_POST["num2"];
		
		if ($num1 < $num2) {
		    $menor = $num1;
		} else {
		    $menor = $num2;
		}

		echo "<br><br>El número menor es: " . $menor;
	}
	?>
</body>
</html>
