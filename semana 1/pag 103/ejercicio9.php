<!DOCTYPE html>
<html>
<head>
	<title>Comprobar si tres longitudes forman un triángulo</title>
</head>
<body>
	<h1>Comprobar si tres longitudes forman un triángulo</h1>

	<form method="post">
		<label for="lado1">Lado 1:</label>
		<input type="number" name="lado1" required><br>

		<label for="lado2">Lado 2:</label>
		<input type="number" name="lado2" required><br>

		<label for="lado3">Lado 3:</label>
		<input type="number" name="lado3" required><br>

		<input type="submit" value="Comprobar">
	</form>

	<?php
	if (isset($_POST['lado1']) && isset($_POST['lado2']) && isset($_POST['lado3'])) {
		$lado1 = $_POST['lado1'];
		$lado2 = $_POST['lado2'];
		$lado3 = $_POST['lado3'];

		if (($lado1 + $lado2 > $lado3) && ($lado1 + $lado3 > $lado2) && ($lado2 + $lado3 > $lado1)) {
			echo "<p>Se puede formar un triángulo con los lados $lado1, $lado2 y $lado3.</p>";
		} else {
			echo "<p>No se puede formar un triángulo con los lados $lado1, $lado2 y $lado3.</p>";
		}
	}
	?>
</body>
</html>
