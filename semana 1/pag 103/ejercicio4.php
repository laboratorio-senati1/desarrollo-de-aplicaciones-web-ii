<!DOCTYPE html>
<html>
<head>
	<title>Doble o Triple de un número</title>
</head>
<body>
	<form method="post">
		<label for="numero">Ingresa un número entero:</label>
		<input type="number" id="numero" name="numero" required>
		<br><br>
		<button type="submit">Calcular</button>
	</form>
	<br>
	<?php
		if(isset($_POST["numero"])) {
			$numero = $_POST["numero"];
			if($numero > 0) {
				$resultado = $numero * 2;
				echo "El doble de $numero es $resultado";
			} else if($numero < 0) {
				$resultado = $numero * 3;
				echo "El triple de $numero es $resultado";
			} else {
				echo "El número es neutro y su valor es cero.";
			}
		}
	?>
</body>
</html>
