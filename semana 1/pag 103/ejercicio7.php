<!DOCTYPE html>
<html>
<head>
	<title>Saldo Actual</title>
</head>
<body>
	<h1>Saldo Actual</h1>
	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		Saldo anterior: <input type="number" name="saldo_anterior"><br><br>
		Tipo de movimiento (R o D): <input type="text" name="tipo_movimiento"><br><br>
		Monto de la transacción: <input type="number" name="monto_transaccion"><br><br>
		<input type="submit" name="submit" value="Calcular">
	</form>

	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$saldo_anterior = $_POST["saldo_anterior"];
			$tipo_movimiento = $_POST["tipo_movimiento"];
			$monto_transaccion = $_POST["monto_transaccion"];

			if ($tipo_movimiento == "R") {
				$saldo_actual = $saldo_anterior - $monto_transaccion;
			} elseif ($tipo_movimiento == "D") {
				$saldo_actual = $saldo_anterior + $monto_transaccion;
			}

			echo "<br>Saldo actual: " . $saldo_actual;
		}
	?>
</body>
</html>
