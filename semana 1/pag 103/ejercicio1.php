<?php
    $edad = $_POST["edad"];
if ($edad >= 18) {
  $mensaje = "La persona es mayor de edad";
} else {
  $mensaje = "La persona es menor de edad";
}
?>
<html>
<head>
	<title>Verificar si una persona es mayor o menor de edad</title>
</head>
<body>
	<h1>Verificar si una persona es mayor o menor de edad</h1>

	<form method="post" action="">
		<label for="edad">Ingresa la edad de la persona:</label>
		<input type="number" name="edad" id="edad" required>
		<br><br>
		<input type="submit" value="Verificar">
	</form>

	<?php
	if (isset($mensaje)) {
	  echo "<br><br><strong>" . $mensaje . "</strong>";
	}
	?>
</body>
</html>
