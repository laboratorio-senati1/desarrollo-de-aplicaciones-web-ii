<html>
<head>
	<title>Conversión de grados Fahrenheit a Celsius y Kelvin</title>
</head>
<body>
	<h1>Conversión de grados Fahrenheit a Celsius y Kelvin</h1>

	<form method="post" action="">
		<label for="fahrenheit">Ingresa la cantidad de grados Fahrenheit:</label>
		<input type="number" name="fahrenheit" id="fahrenheit" required>
		<br><br>
		<input type="submit" value="Convertir">
	</form>

	<?php
	if(isset($_POST['fahrenheit'])){
		$fahrenheit = $_POST['fahrenheit'];
		$celsius = ($fahrenheit - 32) * 5/9;
		$kelvin = $celsius + 273.15;
		echo "<br>Equivalente en Celsius: " . $celsius . "°C";
		echo "<br>Equivalente en Kelvin: " . $kelvin . "K";
	}
	?>
</body>
</html>
