<head>
	<title>Conversor de milímetros</title>
	<style>
		body {
			font-family: Arial, sans-serif;
			background-color: #B9EE88;
		}
		h1 {
			text-align: center;
			color: #333;
		}
		form {
			width: 50%;
			margin: auto;
			background-color: #fff;
			padding: 20px;
			border-radius: 10px;
			box-shadow: 0 0 10px rgba(0,0,0,0.2);
		}
		label {
			display: block;
			margin-bottom: 10px;
			color: #333;
		}
		input[type="number"] {
			padding: 10px;
			border: none;
			background-color: #eee;
			border-radius: 5px;
			margin-bottom: 20px;
			font-size: 16px;
		}
		input[type="submit"] {
			padding: 10px 20px;
			background-color: #333;
			color: #fff;
			border: none;
			border-radius: 5px;
			font-size: 16px;
			cursor: pointer;
		}
		p {
			margin-top: 20px;
			color: #333;
			font-size: 18px;
			text-align: center;
		}
	</style>
</head>
<body>
	<h1>Conversor de milímetros</h1>
	<form method="post">
		<label for="mm">Ingrese la cantidad de milímetros:</label>
		<input type="number" id="mm" name="mm" required>
		<input type="submit" value="Convertir">
	</form>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$mm = intval($_POST["mm"]);

	$m = floor($mm / 1000);
	$mm %= 1000;
	$dm = floor($mm / 100);
	$mm %= 100;
	$cm = floor($mm / 10);
	$mm %= 10;

    echo "<p>$m metros, $dm decímetros, $cm centímetros y $mm milímetros</p>";
}
?>
