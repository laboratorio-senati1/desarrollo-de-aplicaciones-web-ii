<html>
<head>
	<title>Conversión de Grados Sexagesimales a Centesimales</title>
</head>
<body>
	<h1>Conversión de Grados Sexagesimales a Centesimales</h1>

	<form method="post" action="">
		<label for="grados">Ingresa los grados sexagesimales:</label>
		<input type="number" name="grados" id="grados" required>
		<br><br>
		<input type="submit" value="Calcular">
	</form>

	<?php
	if(isset($_POST['grados'])){
		$grados = $_POST['grados'];
		$centesimales = ($grados / 360) * 100;
		echo "<br>Grados centesimales: " . $centesimales;
	}
	?>
</body>
</html>
