<html>
<head>
	<title>Área y perímetro de un cuadrado</title>
</head>
<body>
	<h1>Área y perímetro de un cuadrado</h1>

	<form method="post" action="">
		<label for="lado">Ingresa la medida del lado:</label>
		<input type="number" name="lado" id="lado" required>
		<br><br>
		<input type="submit" value="Calcular">
	</form>

	<?php
	if(isset($_POST['lado'])){
		$lado = $_POST['lado'];
		$area = pow($lado, 2);
		$perimetro = 4 * $lado;
		echo "<br>El área del cuadrado es: " . $area;
		echo "<br>El perímetro del cuadrado es: " . $perimetro;
	}
	?>
</body>
</html>
