<html>
<head>
	<title>Porcentaje de números</title>
</head>
<body>
	<h1>Porcentaje de números</h1>

	<form method="post">
		<label for="num1">Número 1:</label>
		<input type="number" id="num1" name="num1"><br>

		<label for="num2">Número 2:</label>
		<input type="number" id="num2" name="num2"><br>

		<label for="num3">Número 3:</label>
		<input type="number" id="num3" name="num3"><br>

		<label for="num4">Número 4:</label>
		<input type="number" id="num4" name="num4"><br>

		<input type="submit" value="Calcular">
	</form>

	<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$num1 = $_POST["num1"];
		$num2 = $_POST["num2"];
		$num3 = $_POST["num3"];
		$num4 = $_POST["num4"];
	} else {
		$num1 = 10;
		$num2 = 20;
		$num3 = 30;
		$num4 = 40;
	}

	$sum = $num1 + $num2 + $num3 + $num4;

	$percent1 = ($num1 / $sum) * 100;
	$percent2 = ($num2 / $sum) * 100;
	$percent3 = ($num3 / $sum) * 100;
	$percent4 = ($num4 / $sum) * 100;

	echo "<h2>Resultados:</h2>";
	echo "<table>";
	echo "<tr><th>Número</th><th>Porcentaje</th></tr>";
	echo "<tr><td>$num1</td><td>" . round($percent1, 2) . "%</td></tr>";
	echo "<tr><td>$num2</td><td>" . round($percent2, 2) . "%</td></tr>";
	echo "<tr><td>$num3</td><td>" . round($percent3, 2) . "%</td></tr>";
	echo "<tr><td>$num4</td><td>" . round($percent4, 2) . "%</td></tr>";
	echo "</table>";
	?>
</body>
</html>
