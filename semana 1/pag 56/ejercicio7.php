<html>
<head>
	<title>Conversión de horas a minutos y segundos</title>
</head>
<body>
	<h1>Conversión de horas a minutos y segundos</h1>

	<form method="post" action="">
		<label for="horas">Ingresa la cantidad de horas:</label>
		<input type="number" name="horas" id="horas" required>
		<br><br>
		<input type="submit" value="Convertir">
	</form>

	<?php
	if(isset($_POST['horas'])){
		$horas = $_POST['horas'];
		$minutos = $horas * 60;
		$segundos = $horas * 3600;
		echo "<br>Equivalente en minutos: " . $minutos;
		echo "<br>Equivalente en segundos: " . $segundos;
	}
	?>
</body>
</html>
