<html>
<head>
	<title>Calcular valores de c y d</title>
</head>
<body>
	<form method="post" action="">
		<label for="a">Valor de a:</label>
		<input type="number" name="a" id="a" required><br><br>

		<label for="b">Valor de b:</label>
		<input type="number" name="b" id="b" required><br><br>

		<input type="submit" value="Calcular"><br><br>
	</form>

	<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$a = $_POST["a"];
		$b = $_POST["b"];

		$c = (4 * pow($a, 4) + 3 * $b * $a + pow($b, 2)) / (pow($a, 2) - pow($b, 2));
		$d = (3 * pow($c, 2) + $a + $b) / 4;

		echo "<p>El valor de c es: " . $c . "</p>";
		echo "<p>El valor de d es: " . $d . "</p>";
	}
	?>
</body>
</html>
