<html>
<head>
	<title>Área y Perímetro de un Rectángulo</title>
</head>
<body>
	<h1>Área y Perímetro de un Rectángulo</h1>

	<form method="post" action="">
		<label for="base">Ingresa la base del rectángulo:</label>
		<input type="number" name="base" id="base" required>
		<br><br>
		<label for="altura">Ingresa la altura del rectángulo:</label>
		<input type="number" name="altura" id="altura" required>
		<br><br>
		<input type="submit" value="Calcular">
	</form>

	<?php
	if(isset($_POST['base']) && isset($_POST['altura'])){
		$base = $_POST['base'];
		$altura = $_POST['altura'];
		$area = $base * $altura;
		$perimetro = 2 * ($base + $altura);
		echo "<br>Área del rectángulo: " . $area;
		echo "<br>Perímetro del rectángulo: " . $perimetro;
	}
	?>
</body>
</html>
