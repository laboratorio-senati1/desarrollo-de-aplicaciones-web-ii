<html>
<head>
	<title>Contador de números enteros</title>
</head>
<body>
<style>
		body {
			font-family: Arial, sans-serif;
			background-color: #B9EE88;
		}
		h1 {
			text-align: center;
			color: #333;
		}
		form {
			width: 50%;
			margin: auto;
			background-color: #fff;
			padding: 20px;
			border-radius: 10px;
			box-shadow: 0 0 10px rgba(0,0,0,0.2);
		}
		label {
			display: block;
			margin-bottom: 10px;
			color: #333;
		}
		input[type="number"] {
			padding: 10px;
			border: none;
			background-color: #eee;
			border-radius: 5px;
			margin-bottom: 20px;
			font-size: 16px;
		}
		input[type="submit"] {
			padding: 10px 20px;
			background-color: #333;
			color: #fff;
			border: none;
			border-radius: 5px;
			font-size: 16px;
			cursor: pointer;
		}
		p {
			margin-top: 20px;
			color: #333;
			font-size: 18px;
			text-align: center;
		}
	</style>    
	<h1>Contador de números enteros</h1>
	<form method="post">
		<label for="num1">Ingrese el primer numero entero:</label>
		<input type="number" id="num1" name="num1" required>
		<br>
		<label for="num2">Ingrese el segundo numero entero:</label>
		<input type="number" id="num2" name="num2" required>
		<br>
		<input type="submit" value="Contar">
	</form>

	<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$a = intval($_POST["num1"]);
		$b = intval($_POST["num2"]);

        if ($a > $b) {
			$temp = $a;
			$a = $b;
			$b = $temp;
		}

		$count = 0;

		for ($i = $a; $i <= $b; $i++) {
			if (is_int($i)) {
				$count++;
			}
		}

		echo "<p>Hay $count números enteros incluidos entre $a y $b.</p>";
	}
	?>
</body>
</html>