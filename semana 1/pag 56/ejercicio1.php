<form method="post">
  <label for="a">Ingrese el primer número:</label>
  <input type="number" id="a" name="a"><br>

  <label for="b">Ingrese el segundo número:</label>
  <input type="number" id="b" name="b"><br>

  <button type="submit">Calcular</button>
</form>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $a = $_POST['a'];
  $b = $_POST['b'];

  $suma = $a + $b;
  echo "La suma de $a y $b es: $suma<br>";

  $resta = $a - $b;
  echo "La resta de $a y $b es: $resta";
}
?>


