<html>
<head>
	<title>Números capicúa</title>
</head>
<body>
	<h1>Números capicúa</h1>
	<form method="post">
		<label for="inicio">Inicio del rango:</label>
		<input type="number" name="inicio" required><br>
		<label for="fin">Fin del rango:</label>
		<input type="number" name="fin" required><br>
		<input type="submit" value="Calcular">
	</form>
	<?php
		function es_capicua($numero) {
			$numero_invertido = strrev($numero);
			return $numero == $numero_invertido;
		}

		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$inicio = $_POST["inicio"];
			$fin = $_POST["fin"];
			$numeros_capicua = 0;

			for ($i = $inicio; $i <= $fin; $i++) {
				if (es_capicua($i)) {
					$numeros_capicua++;
				}
			}

			 $porcentaje_capicua = ($numeros_capicua / ($fin - $inicio + 1)) * 100;
			 $porcentaje_no_capicua = 100 - $porcentaje_capicua;

			echo "<p>Hay $numeros_capicua números capicúa en el rango del $inicio al $fin.</p>";
			echo "<p>El $porcentaje_capicua % de los números son capicúa.</p>";
			echo "<p>El $porcentaje_no_capicua % de los números no son capicúa.</p>";
		}
	?>
</body>
</html>
