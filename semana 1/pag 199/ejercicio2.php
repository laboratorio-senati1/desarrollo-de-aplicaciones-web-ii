<!DOCTYPE html>
<html>
<head>
	<title>Números pares e impares</title>
</head>
<body>
	<form method="post">
		<label for="inicio">Inicio:</label>
		<input type="number" name="inicio" id="inicio">
		<label for="fin">Fin:</label>
		<input type="number" name="fin" id="fin">
		<input type="submit" value="Calcular">
	</form>

	<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$inicio = $_POST["inicio"];
		$fin = $_POST["fin"];
		$pares = 0;
		$impares = 0;

		for ($i = $inicio; $i <= $fin; $i++) {
			if ($i % 5 == 0) {
				continue;
			}

			if ($i % 2 == 0) {
				$pares++;
			} else {
				$impares++;
			}
		}

		echo "<p>Entre $inicio y $fin, hay $pares números pares y $impares números impares, sin contar los múltiplos de 5.</p>";
	}
	?>
</body>
</html>
