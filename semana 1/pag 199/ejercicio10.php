<html>
<head>
	<title>Máximo Común Divisor</title>
</head>
<body>
	<form method="post" action="">
		<label>Ingrese el primer número:</label><br>
		<input type="number" name="num1" required><br><br>
		<label>Ingrese el segundo número:</label><br>
		<input type="number" name="num2" required><br><br>
		<input type="submit" name="submit" value="Calcular MCD">
	</form>

	<?php
		if(isset($_POST['submit'])) {
			$num1 = $_POST['num1'];
			$num2 = $_POST['num2'];

			$min = min($num1, $num2);
			$max = max($num1, $num2);

			$divisor = 2;
			$MCD = 1;

			while($divisor <= $min) {
				if($min % $divisor == 0 && $max % $divisor == 0) {
					$MCD *= $divisor;
					$min /= $divisor;
					$max /= $divisor;
				} else {
					$divisor++;
				}
			}

			echo "<br><br>El MCD de $num1 y $num2 es: " . $MCD;
		}
	?>
</body>
</html>
