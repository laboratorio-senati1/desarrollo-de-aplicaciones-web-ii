<!DOCTYPE html>
<html>
<head>
	<title>Suma y producto de los N primeros múltiplos de 3</title>
</head>
<body>
	<form method="post">
		<label for="n">Ingrese un número entero positivo:</label>
		<input type="number" name="n" required>
		<button type="submit">Calcular</button>
	</form>

	<?php
	if (isset($_POST['n'])) {
		$n = (int)$_POST['n'];
		if ($n <= 0) {
			echo "<p>El número ingresado debe ser positivo.</p>";
		} else {
			$suma = 0;
			$producto = 1;
			$multiplo = 3;
			for ($i = 1; $i <= $n; $i++) {
				$suma += $multiplo;
				$producto *= $multiplo;
				$multiplo += 3;
			}
			echo "<p>La suma de los $n primeros múltiplos de 3 es: $suma</p>";
			echo "<p>El producto de los $n primeros múltiplos de 3 es: $producto</p>";
		}
	}
	?>
</body>
</html>
