<!DOCTYPE html>
<html>
<head>
	<title>Dígitos 0</title>
</head>
<body>
	<form method="post">
		<label for="numero">Ingrese un número:</label>
		<input type="number" id="numero" name="numero" required>
		<input type="submit" value="Contar dígitos 0">
	</form>
	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$numero = $_POST["numero"];
			$numero_string = (string) $numero; 
			$cantidad_ceros = 0;
			for ($i = 0; $i < strlen($numero_string); $i++) {
				if ($numero_string[$i] == "0") {
					$cantidad_ceros++;
				}
			}
			echo "<p>El número $numero contiene $cantidad_ceros dígitos 0.</p>";
		}
	?>
</body>
</html>
