<html>
<head>
    <title>Multiplicación de matrices</title>
</head>
<body>

<?php
$A = array(array(1, 2), array(3, 4));

$B = array(array(5, 6), array(7, 8));

$C = array(array(0, 0), array(0, 0));

for ($i = 0; $i < 2; $i++) {
    for ($j = 0; $j < 2; $j++) {
        for ($k = 0; $k < 2; $k++) {
            $C[$i][$j] += $A[$i][$k] * $B[$k][$j];
        }
    }
}

echo "<h2>Matriz C = A * B:</h2>";
echo "<table border='1'>";
for ($i = 0; $i < 2; $i++) {
    echo "<tr>";
    for ($j = 0; $j < 2; $j++) {
        echo "<td>" . $C[$i][$j] . "</td>";
    }
    echo "</tr>";
}
echo "</table>";
?>

</body>
</html>
