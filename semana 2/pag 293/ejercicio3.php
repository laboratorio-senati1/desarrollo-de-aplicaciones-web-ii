<?php
if(isset($_POST['submit'])) {
  $numeros = $_POST['numeros'];
  $n = $_POST['n'];

  $numeros = explode(",", $numeros);
  $multiplos = 0;
  foreach ($numeros as $numero) {
    if ($numero % $n == 0) {
      $multiplos++;
    }
  }
}
?>

<html>
<head>
  <title>Números múltiplos de n</title>
</head>
<body>
  <h1>Números múltiplos de n</h1>
  <form method="POST" action="">
    <label>Ingrese 6 números separados por comas:</label>
    <input type="text" name="numeros">
    <br>
    <label>Ingrese el valor de n:</label>
    <input type="number" name="n">
    <br>
    <button type="submit" name="submit">Calcular</button>
  </form>

  <?php if(isset($_POST['submit'])) { ?>
    <p>Los números ingresados son: <?php echo implode(", ", $numeros); ?></p>
    <p>El número de múltiplos de <?php echo $n; ?> es: <?php echo $multiplos; ?></p>
  <?php } ?>
</body>
</html>
