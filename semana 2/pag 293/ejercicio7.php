<html>
<head>
	<title>Promedio aritmético de una matriz</title>
</head>
<body>

	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<label for="num1">Número 1:</label>
		<input type="number" name="num1" id="num1"><br>

		<label for="num2">Número 2:</label>
		<input type="number" name="num2" id="num2"><br>

		<label for="num3">Número 3:</label>
		<input type="number" name="num3" id="num3"><br>

		<label for="num4">Número 4:</label>
		<input type="number" name="num4" id="num4"><br>

		<label for="num5">Número 5:</label>
		<input type="number" name="num5" id="num5"><br>

		<label for="num6">Número 6:</label>
		<input type="number" name="num6" id="num6"><br>

		<input type="submit" name="submit" value="Calcular promedio aritmético">
	</form>

	<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$matriz = array(array($_POST["num1"], $_POST["num2"]), array($_POST["num3"], $_POST["num4"]), array($_POST["num5"], $_POST["num6"]));

		$suma = 0;
		foreach ($matriz as $fila) {
			foreach ($fila as $elemento) {
				$suma += $elemento;
			}
		}

		$promedio = $suma / 6;

		echo "<p>El promedio aritmético es: " . $promedio . "</p>";
	}
	?>

</body>
</html>
