<?php
if(isset($_POST['submit'])) {
  $numeros = $_POST['numeros'];

  $numeros = explode(",", $numeros);

  $repetidos = 0;
  foreach (array_count_values($numeros) as $valor) {
    if ($valor > 1) {
      $repetidos += $valor;
    }
  }
}
?>

<html>
<head>
  <title>Números repetidos</title>
</head>
<body>
  <h1>Números repetidos</h1>
  <form method="POST" action="">
    <label>Ingrese 6 números separados por comas:</label>
    <input type="text" name="numeros">
    <br>
    <button type="submit" name="submit">Calcular</button>
  </form>

  <?php if(isset($_POST['submit'])) { ?>
    <p>Los números ingresados son: <?php echo implode(", ", $numeros); ?></p>
    <p>El número de números repetidos es: <?php echo $repetidos; ?></p>
  <?php } ?>
</body>
</html>
