<?php
if(isset($_POST['submit'])) {
  $numeros = $_POST['numeros'];

  $suma = array_sum($numeros);

  $promedio = $suma / count($numeros);
}
?>

<html>
<head>
  <title>Suma y promedio de números</title>
</head>
<body>
  <h1>Suma y promedio de números</h1>
  <form method="POST" action="">
    <label>Ingrese 4 números separados por comas:</label>
    <input type="text" name="numeros">
    <button type="submit" name="submit">Calcular</button>
  </form>

  <?php if(isset($_POST['submit'])) { ?>
    <p>Los números ingresados son: <?php echo implode(", ", $numeros); ?></p>
    <p>La suma es: <?php echo $suma; ?></p>
    <p>El promedio es: <?php echo $promedio; ?></p>
  <?php } ?>
</body>
</html>
