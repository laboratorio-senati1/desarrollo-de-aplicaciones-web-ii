<html>
<head>
	<title>Verificar Palindromo</title>
</head>
<body>
<style>
        body {
			font-family: Arial, sans-serif;
			background-image: url("https://www.blogdelfotografo.com/wp-content/uploads/2017/01/switzerland-862870_1920.jpg");
			background-color: #cccccc;
			background-size: cover;
		}
		h1 {
			text-align: center;
			color: #FAF9DD;
		}
        h2 {
			text-align: center;
			color: #FAF9DD;
		}
		form {
			width: 50%;
			margin: auto;
			background-color: #fff;
			padding: 20px;
			border-radius: 10px;
			box-shadow: 0 0 10px rgba(0,0,0,0.2);
		}
		label {
			display: block;
			margin-bottom: 10px;
			color: #333;
		}
		input[type="number"] {
			padding: 10px;
			border: none;
			background-color: #eee;
			border-radius: 5px;
			margin-bottom: 20px;
			font-size: 16px;
		}
		input[type="submit"] {
			padding: 10px 20px;
			background-color: #333;
			color: #fff;
			border: none;
			border-radius: 5px;
			font-size: 16px;
			cursor: pointer;
		}
		p {
			margin-top: 20px;
			color: #FAF9DD;
			font-size: 18px;
			text-align: center;
            
		}
    </style>
	<h1>Verificar Palindromo</h1>
	<form method="post">
		<label>Ingrese una palabra:</label>
		<input type="text" name="palabra">
		<button type="submit">Verificar</button>
	</form>
	<?php
		function es_palindromo($palabra) {
			$palabra = strtolower(trim($palabra));
			$longitud = strlen($palabra);
			for ($i = 0; $i < $longitud / 2; $i++) {
				if ($palabra[$i] != $palabra[$longitud - $i - 1]) {
					return false;
				}
			}
			return true;
		}
		if (isset($_POST['palabra'])) {
			$palabra = $_POST['palabra'];
			if (es_palindromo($palabra)) {
				echo "<p>La palabra '$palabra' es un palindromo</p>";
			} else {
				echo "<p>La palabra '$palabra' no es un palindromo</p>";
			}
		}		
	?>
</body>
</html>
