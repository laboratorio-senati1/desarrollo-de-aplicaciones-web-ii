<?php
function descuento($tipo) {
    if ($tipo == 'G') {
        return 0.15;
    } elseif ($tipo == 'A') {
        return 0.20;
    } else {
        return 0;
    }
}

function recargo($tipo) {
    if ($tipo == 'G') {
        return 0.10;
    } elseif ($tipo == 'A') {
        return 0.05;
    } else {
        return 0;
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $tipo_cliente = $_POST['tipo_cliente'];
    $forma_pago = $_POST['forma_pago'];
    $monto = $_POST['monto'];

    $desc = descuento($tipo_cliente);
    $rec = recargo($tipo_cliente);

    if ($forma_pago == 'C') {
        $monto_descuento = $monto * $desc;
        $total = $monto - $monto_descuento;
        $monto_recargo = 0;
    } elseif ($forma_pago == 'P') {
        $monto_recargo = $monto * $rec;
        $total = $monto + $monto_recargo;
        $monto_descuento = 0;
    } else {
        $monto_descuento = 0;
        $monto_recargo = 0;
        $total = $monto;
    }
}
?>
<html>
<head>
    <title>Calculo del Descuento y Recargo</title>
</head>
<body>
    <style>
        body {
			font-family: Arial, sans-serif;
			background-image: url("https://www.blogdelfotografo.com/wp-content/uploads/2017/01/switzerland-862870_1920.jpg");
			background-color: #cccccc;
			background-size: cover;
		}
		h1 {
			text-align: center;
			color: #FAF9DD;
		}
        h2 {
			text-align: center;
			color: #FAF9DD;
		}
		form {
			width: 50%;
			margin: auto;
			background-color: #fff;
			padding: 20px;
			border-radius: 10px;
			box-shadow: 0 0 10px rgba(0,0,0,0.2);
		}
		label {
			display: block;
			margin-bottom: 10px;
			color: #333;
		}
		input[type="number"] {
			padding: 10px;
			border: none;
			background-color: #eee;
			border-radius: 5px;
			margin-bottom: 20px;
			font-size: 16px;
		}
		input[type="submit"] {
			padding: 10px 20px;
			background-color: #333;
			color: #fff;
			border: none;
			border-radius: 5px;
			font-size: 16px;
			cursor: pointer;
		}
		p {
			margin-top: 20px;
			color: #FAF9DD;
			font-size: 18px;
			text-align: center;
            
		}
    </style>
    <h1>Calculo del Descuento y Recargo</h1>
    <form method="post">
        <label>Tipo de cliente:</label>
        <select name="tipo_cliente">
            <option value="G">Público en general</option>
            <option value="A">Cliente afiliado</option>
        </select><br><br>
        <label>Forma de pago:</label>
        <select name="forma_pago">
            <option value="C">Al contado</option>
            <option value="P">En plazos</option>
        </select><br><br>
        <label>Monto de la compra:</label>
        <input type="number" name="monto"><br><br>
        <input type="submit" value="Calcular">
    </form>
    <?php if ($_SERVER['REQUEST_METHOD'] == 'POST'): ?>
    <h2>Resultado:</h2>
    <p>Monto de la compra: S/<?= $monto ?></p>
    <?php if ($monto_descuento > 0): ?>
    <p>Descuento (<?= $desc * 100 ?>%): S/<?= $monto_descuento ?></p>
    <?php endif; ?>
    <?php if ($monto_recargo > 0): ?>
    <p>Recargo (<?= $rec * 100 ?>%): S/<?= $monto_recargo ?></p>
    <?php endif; ?>
    <p>Total a pagar: S/<?= $total ?></p>
    <?php endif; ?>
</body>
</html>
