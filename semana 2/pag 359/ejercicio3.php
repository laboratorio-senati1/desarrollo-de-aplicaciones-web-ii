<?php
function obtenerEtapa($edad) {
    if ($edad >= 0 && $edad <= 2) {
        return "Bebé";
    } elseif ($edad >= 3 && $edad <= 5) {
        return "Niño";
    } elseif ($edad >= 6 && $edad <= 12) {
        return "Pubertad";
    } elseif ($edad >= 13 && $edad <= 18) {
        return "Adolescente";
    } elseif ($edad >= 19 && $edad <= 25) {
        return "Joven";
    } elseif ($edad >= 26 && $edad <= 60) {
        return "Adulto";
    } elseif ($edad >= 61 && $edad <= 99){
        return "Anciano";  
    } elseif ($edad >= 100 && $edad <= 199){
        return "El mas cercano a buda";      
    } else {
        return "Inmortal";
    }
}

if (isset($_POST['edad'])) {
    $edad = $_POST['edad'];
    $etapa = obtenerEtapa($edad);
} else {
    $edad = '';
    $etapa = '';
}
?>
<html>
<head>
    <title>Etapa de vida</title>
</head>
<body>
<style>
		body {
			font-family: Arial, sans-serif;
			background-image: url("https://www.blogdelfotografo.com/wp-content/uploads/2017/01/switzerland-862870_1920.jpg");
			background-color: #cccccc;
			background-size: cover;
		}
		h1 {
			text-align: center;
			color: #FAF9DD;
		}
		form {
			width: 50%;
			margin: auto;
			background-color: #fff;
			padding: 20px;
			border-radius: 10px;
			box-shadow: 0 0 10px rgba(0,0,0,0.2);
		}
		label {
			display: block;
			margin-bottom: 10px;
			color: #333;
		}
		input[type="number"] {
			padding: 10px;
			border: none;
			background-color: #eee;
			border-radius: 5px;
			margin-bottom: 20px;
			font-size: 16px;
		}
		input[type="submit"] {
			padding: 10px 20px;
			background-color: #333;
			color: #fff;
			border: none;
			border-radius: 5px;
			font-size: 16px;
			cursor: pointer;
		}
		p {
			margin-top: 20px;
			color: #FAF9DD;
			font-size: 18px;
			text-align: center;
            
		}
	</style>
    <h1>Etapa de vida</h1>
    <form method="POST">
        <label for="edad">Ingrese su edad:</label>
        <input type="number" name="edad" id="edad" value="<?php echo $edad; ?>">
        <input type="submit" value="Calcular">
    </form>
    <?php if ($etapa != ''): ?>
        <p>Su etapa de la vida es: <?php echo $etapa; ?></p>
    <?php endif; ?>
</body>
</html>
