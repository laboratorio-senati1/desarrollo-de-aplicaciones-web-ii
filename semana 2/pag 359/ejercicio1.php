<html>
<head>
	<title>Área y Perímetro de un Cuadrado</title>
</head>
<body>
	<style>
		body {
			font-family: Arial, sans-serif;
			background-image: url("https://www.blogdelfotografo.com/wp-content/uploads/2017/01/switzerland-862870_1920.jpg");
			background-color: #cccccc;
			background-size: cover;
		}
		h1 {
			text-align: center;
			color: #FAF9DD;
		}
		form {
			width: 50%;
			margin: auto;
			background-color: #fff;
			padding: 20px;
			border-radius: 17px;
			box-shadow: 0 0 10px rgba(0,0,0,0.2);
		}
		label {
			display: block;
			margin-bottom: 10px;
			color: #333;
		}
		input[type="number"] {
			padding: 10px;
			border: none;
			background-color: #eee;
			border-radius: 5px;
			margin-bottom: 20px;
			font-size: 16px;
		}
		input[type="submit"] {
			padding: 10px 20px;
			background-color: #333;
			color: #fff;
			border: none;
			border-radius: 5px;
			font-size: 16px;
			cursor: pointer;
		}
		p {
			margin-top: 20px;
			color: #FAF9DD;
			font-size: 18px;
			text-align: center;
            
		}
	</style>
	<h1>Área y Perímetro de un Cuadrado</h1>
	<form method="post">
		<label for="lado">Ingrese el lado del cuadrado:</label>
		<input type="number" name="lado" id="lado" required>
		<button type="submit">Calcular</button>
	</form>
	<?php
		if ($_POST) {
			$lado = $_POST['lado'];
			$area = pow($lado, 2);
			$perimetro = $lado * 4;
			echo "<p>El área del cuadrado es: $area</p>";
			echo "<p>El perímetro del cuadrado es: $perimetro</p>";
		}
	?>
</body>
</html>
