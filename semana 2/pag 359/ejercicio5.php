<?php
function determinarTipoCaracter($caracter, &$tipo) {
    if (ctype_alpha($caracter)) {
        if (ctype_upper($caracter)) {
            $tipo = "letra mayúscula";
        } else {
            $tipo = "letra minúscula";
        }
        if (in_array($caracter, array('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'))) {
            $tipo = "vocal";
        }
    } elseif (ctype_digit($caracter)) {
        $tipo = "número";
    } else {
        $tipo = "símbolo";
    }
}
?>
<html>
<head>
	<title>Determinar tipo de carácter</title>
</head>
<body>
    <style>
        body {
			font-family: Arial, sans-serif;
			background-image: url("https://www.blogdelfotografo.com/wp-content/uploads/2017/01/switzerland-862870_1920.jpg");
			background-color: #cccccc;
			background-size: cover;
		}
		h1 {
			text-align: center;
			color: #FAF9DD;
		}
		form {
			width: 50%;
			margin: auto;
			background-color: #fff;
			padding: 20px;
			border-radius: 10px;
			box-shadow: 0 0 10px rgba(0,0,0,0.2);
		}
		label {
			display: block;
			margin-bottom: 10px;
			color: #333;
		}
		input[type="number"] {
			padding: 10px;
			border: none;
			background-color: #eee;
			border-radius: 5px;
			margin-bottom: 20px;
			font-size: 16px;
		}
		input[type="submit"] {
			padding: 10px 20px;
			background-color: #333;
			color: #fff;
			border: none;
			border-radius: 5px;
			font-size: 16px;
			cursor: pointer;
		}
		p {
			margin-top: 20px;
			color: #FAF9DD;
			font-size: 18px;
			text-align: center;
            
		}
    </style>
    <h1>Los Caracteres</h1>
	<form method="post">
		<label for="caracter">Ingresa un carácter:</label>
		<input type="text" id="caracter" name="caracter" maxlength="1" required><br>
		<input type="submit" value="Determinar">
	</form>

	<?php
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$char = $_POST['caracter'];
		$tipo = "";
		determinarTipoCaracter($char, $tipo);
		echo "<p>El carácter '$char' es un $tipo</p>";
	}
	?>
</body>
</html>
