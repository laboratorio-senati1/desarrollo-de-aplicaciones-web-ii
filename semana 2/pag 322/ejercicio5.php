
<html>
<head>
	<title>Encriptar frase</title>
</head>
<body>
	<form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<label for="frase">Ingrese una frase:</label>
		<input type="text" id="frase" name="frase"><br>
		<input type="submit" value="Encriptar frase">
	</form>
</body>
</html>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $frase = $_POST["frase"];
    $frase_encriptada = "";

    for ($i = 0; $i < strlen($frase); $i++) {
        $ascii = ord($frase[$i]);
        $ascii_modificado = ($ascii + 2) % 256;
        $caracter_encriptado = chr($ascii_modificado);
        $frase_encriptada .= $caracter_encriptado;
    }

    echo "La frase encriptada es: " . $frase_encriptada;
}
?>