<html>
<head>
	<title>Contar palabras palíndromas</title>
</head>
<body>
	
	<form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<label for="frase">Ingrese una frase:</label>
		<input type="text" id="frase" name="frase"><br>
		<input type="submit" value="Contar palabras palíndromas">
	</form>
</body>
</html>
<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$frase = $_POST["frase"];
			$palabras = explode(" ", $frase);
			$contador_palindromos = 0;
			foreach ($palabras as $palabra) {
				if (strtolower($palabra) == strtolower(strrev($palabra))) {
					$contador_palindromos++;
				}
			}
			echo "La frase tiene <b>$contador_palindromos</b> palabra(s) palíndromas.";
		}
	?>