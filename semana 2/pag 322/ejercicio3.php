<html>
<head>
	<title>Buscar palabra en una frase</title>
</head>
<body>
	
	<form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<label for="frase">Ingrese la frase:</label>
		<input type="text" id="frase" name="frase"><br>
		<label for="palabra">Ingrese la palabra a buscar:</label>
		<input type="text" id="palabra" name="palabra"><br>
		<input type="submit" value="Buscar">
	</form>
</body>
</html>
<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$frase = $_POST["frase"];
			$palabra = $_POST["palabra"];
			if (strpos($frase, $palabra) !== false) {
				echo "La palabra <b>$palabra</b> está presente en la frase <b>$frase</b>.";
			} else {
				echo "La palabra <b>$palabra</b> no está presente en la frase <b>$frase</b>.";
			}
		}
	?>